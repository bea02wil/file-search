﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Timers;

namespace FileSearch
{
    public partial class FileFinder : Form
    {
        public FileFinder()
        {
            InitializeComponent();                    
        }
    
        private FolderBrowserDialog ShowFolder = new FolderBrowserDialog();
        private ManualResetEvent busy = new ManualResetEvent(false);               
        private int min, sec, hr;
        private System.Timers.Timer timer;       

        private void textBox1_Click(object sender, EventArgs e)
        {           
            if (ShowFolder.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = ShowFolder.SelectedPath;              
            }
        }      
        
        private void SaveCriteria()
        {
            using (BinaryWriter info = new BinaryWriter(File.Create("criteria.bin")))
            {
                info.Write(ShowFolder.SelectedPath);
                info.Write(textBox2.Text);
                info.Write(textBox3.Text);
            }
        } 

        private void LoadCriteria()
        {
            try
            {
                using (BinaryReader info = new BinaryReader(File.OpenRead("criteria.bin")))
                {
                    string showFolderDirectory = info.ReadString();
                    string fileNamePattern = info.ReadString();
                    string textToSearch = info.ReadString();

                    ShowFolder.SelectedPath = showFolderDirectory;
                    textBox1.Text = ShowFolder.SelectedPath;
                    textBox2.Text = fileNamePattern;
                    textBox3.Text = textToSearch;                  
                }
            }
            catch (Exception)
            {

            }
        }

        private void WorkerMod(bool running)
        {
            if (running)
            {
                startBtn.Text = "Остановить";
                busy.Set();
            }
            else
            {
                startBtn.Text = "Продолжить";
                busy.Reset();
            }
        }       

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {                       
            progressBar.Value = e.ProgressPercentage;           
            currentprogress.Text = string.Format($"Выполнен на {e.ProgressPercentage}%");                                           
            progressBar.Update();           
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {                      
            if (e.Cancelled == true)
            {
                timer.Stop();
                treeView1.Nodes.Clear();
                listBox1.Items.Clear();

                MessageBox.Show("Поиск прерван", "Отменено", MessageBoxButtons.OK, MessageBoxIcon.Information);

                progressBar.Value = 0;
                currentprogress.Text = "Поиск файлов";
                currentfile.Text = "Файл:";
                countfileslbl.Text = "";
                lbltime.Text = "00:00:00";
            }
            else
            {
                startBtn.Text = "Запустить поиск";
                timer.Stop();
                listBox1.Items.Clear();

                MessageBox.Show("Поиск выполнен", "", MessageBoxButtons.OK, MessageBoxIcon.Information);               
            }
        }
      
        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            string textToSearch = textBox3.Text;
            string filePatternName = textBox2.Text;
            string Dir = ShowFolder.SelectedPath;
            int index = 1;
            int totalNumbersOfPatternNameWithOutOftextToSearch = SearchHelpers.CountFiles(Dir, filePatternName);
            BackgroundWorker worker = sender as BackgroundWorker;
                       
            Invoke(new Action(() => SearchHelpers.ListContainer(Dir, filePatternName, listBox1)));
           
            for (int i = 1; i <= totalNumbersOfPatternNameWithOutOftextToSearch; i++)
            {
                if (worker.CancellationPending == true)
                {
                    e.Cancel = true;
                }
                else
                {                   
                   worker.ReportProgress(index++ * 100 / totalNumbersOfPatternNameWithOutOftextToSearch);               
                   busy.WaitOne();
                   Thread.Sleep(50);                
                   if (this.InvokeRequired)
                   {                      
                       Invoke(new Action(() => countfileslbl.Text = i.ToString()));
                       Invoke(new Action(() => ShowPath(i - 1)));                                                                                          
                   }                   
                }
            }
            Invoke(new Action(() => SearchHelpers.LoadDirectory(Dir, treeView1, textToSearch, filePatternName)));
        }

        private void stopBtn_Click(object sender, EventArgs e)
        {
            if (startBtn.Text == "Продолжить")
            {
                busy.Set();
                backgroundWorker.CancelAsync();
            }
            if (backgroundWorker.WorkerSupportsCancellation == true)
            {              
                startBtn.Text = "Запустить поиск";               
                backgroundWorker.CancelAsync();               
            }
        }

        private void startBtn_Click(object sender, EventArgs e)
        {            
            treeView1.Nodes.Clear();
           
            string fileNamePattern = textBox2.Text;
            string textInFile = textBox3.Text;
            string Dir = ShowFolder.SelectedPath;

            if (Dir == "" || fileNamePattern == "" || textInFile == "")
            {               
                MessageBox.Show("Не все поля заполнены!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);               
            }
            
            else if (backgroundWorker.IsBusy != true)
            {
                WorkerMod(true);
                backgroundWorker.RunWorkerAsync();
                timer.Start();
                lbltime.Text = "00:00:00";
            }
            else if (startBtn.Text == "Остановить")
            {
                WorkerMod(false);
                timer.Stop();
            }
            else if (startBtn.Text == "Продолжить")
            {
                startBtn.Text = "Остановить";
                busy.Set();
                timer.Start();
            }                     
        }

        private void TextBox_Leave(object sender, EventArgs e)
        {
            SaveCriteria();
        }
                             
        private void Form1_Load(object sender, EventArgs e)
        {
            LoadCriteria();

            countfileslbl.Text = "";
            lbltime.Text = "";

            timer = new System.Timers.Timer();
            timer.Interval = 1000;
            timer.Elapsed += TimeEvent;
        }
        
        private void TimeEvent(object sender, System.Timers.ElapsedEventArgs e)
        {
            Invoke(new Action(() =>
            {
                if (lbltime.Text == "00:00:00")
                {
                    sec = 0;
                    min = 0;
                    hr = 0;
                }
                sec += 1;
                if (sec == 60)
                {
                    sec = 0;
                    min += 1;
                }
                if (min == 60)
                {
                    min = 0;
                    hr += 1;
                }
                string formatting = string.Format($"{hr.ToString().PadLeft(2, '0')}:{min.ToString().PadLeft(2, '0')}" +
                    $":{sec.ToString().PadLeft(2, '0')}");
                lbltime.Text = formatting;               
            }));
            
        }

        private void ShowPath(int i)
        {       
           currentfile.Text = "Файл: " + listBox1.Items[i];                          
        }            
    }
}
