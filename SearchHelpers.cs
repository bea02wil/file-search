﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace FileSearch
{
    class SearchHelpers
    {     
        public static int CountFiles(string Dir, string filePatternName)
        {
            int filesCounter = 0;
            IEnumerable<string> files = Directory.EnumerateFiles(Dir, filePatternName, SearchOption.AllDirectories);
            foreach (string file in files)                                           
            {
                if (File.Exists(file))
                {
                    filesCounter++;                    
                }
            }
            return filesCounter;  
        }

        public static void ListContainer(string Dir, string filePatternName, ListBox listBox1 )
        {
            try
            {
                IEnumerable<string> files = Directory.EnumerateFiles(Dir, filePatternName, SearchOption.AllDirectories);
                foreach (string file in files)                         
                {
                    listBox1.Items.Add(file);
                }
            }
            catch (UnauthorizedAccessException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public static void LoadDirectory(string Dir, TreeView treeView1, string textToSearch, string filePatternName)
        {
            treeView1.Nodes.Clear();

            DirectoryInfo di = new DirectoryInfo(Dir);
            TreeNode directoryNode = treeView1.Nodes.Add(di.FullName);           
            directoryNode.Tag = di.FullName;
        
            LoadSubDirectories(Dir, directoryNode, textToSearch, filePatternName);          
            LoadFiles(Dir, directoryNode, textToSearch, filePatternName);
        }

        public static void LoadSubDirectories(string Dir, TreeNode directoryNode, string textToSearch, string filePatternName)
        {           
            List<string> directory = new List<string>(Directory.EnumerateDirectories(Dir));            
            foreach (string subdirectory in directory)
            {
                DirectoryInfo di = new DirectoryInfo(subdirectory);
                TreeNode tnd = directoryNode.Nodes.Add(di.Name);             
                tnd.Tag = di.FullName;

                LoadSubDirectories(subdirectory, tnd, textToSearch, filePatternName);
                LoadFiles(subdirectory, tnd, textToSearch, filePatternName);
            }
        }

        public static void LoadFiles(string Dir, TreeNode directoryNode, string textToSearch, string filePatternName)
        {
            IEnumerable<string> files = Directory.EnumerateFiles(Dir, filePatternName);
            foreach (string file in files)
            {
                Encoding asCyrillic = Encoding.GetEncoding(1251);
                Encoding asLatin = Encoding.UTF8;
                string rusFile = File.ReadAllText(file, asCyrillic);
                string engFile = File.ReadAllText(file, asLatin);
                
                if (rusFile.Contains(textToSearch) || engFile.Contains(textToSearch))
                {
                    LoadTreeNode(file, directoryNode);                 
                }
            }
        }
        
        public static void LoadTreeNode(string file, TreeNode directoryNode)
        {
            FileInfo fi = new FileInfo(file);

            TreeNode tnd = directoryNode.Nodes.Add(fi.Name);
            tnd.Tag = fi.FullName;
        }
                 
    }
}
