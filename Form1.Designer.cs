﻿namespace FileSearch
{
    partial class FileFinder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FileFinder));
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.startBtn = new System.Windows.Forms.Button();
            this.stopBtn = new System.Windows.Forms.Button();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.verifiedfiles = new System.Windows.Forms.Label();
            this.timespent = new System.Windows.Forms.Label();
            this.currentprogress = new System.Windows.Forms.Label();
            this.countfileslbl = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.currentfile = new System.Windows.Forms.Label();
            this.lbltime = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(125, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Стартовая директория:";
            // 
            // textBox1
            // 
            this.textBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.textBox1.Location = new System.Drawing.Point(26, 46);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(249, 20);
            this.textBox1.TabIndex = 1;
            this.textBox1.Click += new System.EventHandler(this.textBox1_Click);
            this.textBox1.Leave += new System.EventHandler(this.TextBox_Leave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Шаблон имени файла:";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(26, 90);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(249, 20);
            this.textBox2.TabIndex = 3;
            this.textBox2.Leave += new System.EventHandler(this.TextBox_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(23, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(164, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Текст, содержащийся в файле";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(26, 134);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(249, 20);
            this.textBox3.TabIndex = 5;
            this.textBox3.Leave += new System.EventHandler(this.TextBox_Leave);
            // 
            // startBtn
            // 
            this.startBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.startBtn.Location = new System.Drawing.Point(26, 187);
            this.startBtn.Name = "startBtn";
            this.startBtn.Size = new System.Drawing.Size(88, 36);
            this.startBtn.TabIndex = 6;
            this.startBtn.Text = "Запустить поиск";
            this.startBtn.UseVisualStyleBackColor = true;
            this.startBtn.Click += new System.EventHandler(this.startBtn_Click);
            // 
            // stopBtn
            // 
            this.stopBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.stopBtn.Location = new System.Drawing.Point(187, 187);
            this.stopBtn.Name = "stopBtn";
            this.stopBtn.Size = new System.Drawing.Size(88, 36);
            this.stopBtn.TabIndex = 7;
            this.stopBtn.Text = "Стоп";
            this.stopBtn.UseVisualStyleBackColor = true;
            this.stopBtn.Click += new System.EventHandler(this.stopBtn_Click);
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(384, -3);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(417, 461);
            this.treeView1.TabIndex = 9;
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.WorkerReportsProgress = true;
            this.backgroundWorker.WorkerSupportsCancellation = true;
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            this.backgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_ProgressChanged);
            this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_RunWorkerCompleted);
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(26, 279);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(249, 23);
            this.progressBar.TabIndex = 10;
            // 
            // verifiedfiles
            // 
            this.verifiedfiles.AutoSize = true;
            this.verifiedfiles.Location = new System.Drawing.Point(26, 353);
            this.verifiedfiles.Name = "verifiedfiles";
            this.verifiedfiles.Size = new System.Drawing.Size(117, 13);
            this.verifiedfiles.TabIndex = 12;
            this.verifiedfiles.Text = "Проверенные файлы:";
            // 
            // timespent
            // 
            this.timespent.AutoSize = true;
            this.timespent.Location = new System.Drawing.Point(26, 383);
            this.timespent.Name = "timespent";
            this.timespent.Size = new System.Drawing.Size(110, 13);
            this.timespent.TabIndex = 13;
            this.timespent.Text = "Затраченное время:";
            // 
            // currentprogress
            // 
            this.currentprogress.AutoSize = true;
            this.currentprogress.Location = new System.Drawing.Point(102, 254);
            this.currentprogress.Name = "currentprogress";
            this.currentprogress.Size = new System.Drawing.Size(80, 13);
            this.currentprogress.TabIndex = 14;
            this.currentprogress.Text = "Поиск файлов";
            // 
            // countfileslbl
            // 
            this.countfileslbl.AutoSize = true;
            this.countfileslbl.Location = new System.Drawing.Point(150, 353);
            this.countfileslbl.Name = "countfileslbl";
            this.countfileslbl.Size = new System.Drawing.Size(46, 13);
            this.countfileslbl.TabIndex = 15;
            this.countfileslbl.Text = "счетчик";
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(187, 421);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(71, 17);
            this.listBox1.TabIndex = 17;
            this.listBox1.Visible = false;
            // 
            // currentfile
            // 
            this.currentfile.AutoSize = true;
            this.currentfile.Location = new System.Drawing.Point(26, 323);
            this.currentfile.Name = "currentfile";
            this.currentfile.Size = new System.Drawing.Size(39, 13);
            this.currentfile.TabIndex = 11;
            this.currentfile.Text = "Файл:";
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.Location = new System.Drawing.Point(143, 383);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(55, 13);
            this.lbltime.TabIndex = 18;
            this.lbltime.Text = "00:00:00с";
            // 
            // FileFinder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lbltime);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.countfileslbl);
            this.Controls.Add(this.currentprogress);
            this.Controls.Add(this.timespent);
            this.Controls.Add(this.verifiedfiles);
            this.Controls.Add(this.currentfile);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.stopBtn);
            this.Controls.Add(this.startBtn);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "FileFinder";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "File Finder";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button startBtn;
        private System.Windows.Forms.Button stopBtn;
        private System.Windows.Forms.TreeView treeView1;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Label verifiedfiles;
        private System.Windows.Forms.Label timespent;
        private System.Windows.Forms.Label currentprogress;
        private System.Windows.Forms.Label countfileslbl;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.Label currentfile;
        private System.Windows.Forms.Label lbltime;
    }
}

